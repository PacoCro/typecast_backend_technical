<?php


namespace App\Exception;


use App\Core\Exception\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UnsupportedRequestException
 * @package App\Exception
 */
class UnsupportedRequestException extends ApiException
{
    /**
     * UnsupportedRequestException constructor.
     */
    public function __construct()
    {
        parent::__construct(
            Response::HTTP_NOT_FOUND,
            'UNSUPPORTED_REQUEST_EXCEPTION'
        );
    }
}