<?php


namespace App\Representation;

/**
 * Class PlanetRepresentation
 * @package App\Representation
 */
class PlanetRepresentation
{
    /**
     * @var string|null
     */
    private ?string $name;

    /**
     * @var int|null
     */
    private ?int $rotationPeriod;

    /**
     * @var int|null
     */
    private ?int $orbitalPeriod;

    /**
     * @var int|null
     */
    private ?int $diameter;

    /**
     * @var string|null
     */
    private ?string $climate;

    /**
     * @var string|null
     */
    private ?string $gravity;

    /**
     * @var string|null
     */
    private ?string $terrain;

    /**
     * @var string|null
     */
    private ?string $surfaceWater;

    /**
     * @var int|null
     */
    private ?int $population;

    /**
     * @var array<int, string>|null
     */
    private ?array $residents;

    /**
     * @var array<int, string>|null
     */
    private ?array $films;

    /**
     * @var string|null
     */
    private ?string $createdAt;

    /**
     * @var string|null
     */
    private ?string $updatedAt;

    /**
     * @var string|null
     */
    private ?string $url;

    /**
     * PlanetRepresentation constructor.
     * @param string $name
     * @param int $rotationPeriod
     * @param int $orbitalPeriod
     * @param int $diameter
     * @param string $climate
     * @param string $gravity
     * @param string $terrain
     * @param string $surfaceWater
     * @param int $population
     * @param array<int, string> $residents
     * @param array<int, string> $films
     * @param string $createdAt
     * @param string $updatedAt
     * @param string $url
     */
    public function __construct(
        ?string $name,
        ?int $rotationPeriod,
        ?int $orbitalPeriod,
        ?int $diameter,
        ?string $climate,
        ?string $gravity,
        ?string $terrain,
        ?string $surfaceWater,
        ?int $population,
        ?array $residents,
        ?array $films,
        ?string $createdAt,
        ?string $updatedAt,
        ?string $url
    ) {
        $this->name = $name;
        $this->rotationPeriod = $rotationPeriod;
        $this->orbitalPeriod = $rotationPeriod;
        $this-> orbitalPeriod = $orbitalPeriod;
        $this->diameter = $diameter;
        $this->climate = $climate;
        $this-> gravity = $gravity;
        $this->terrain = $terrain;
        $this->surfaceWater = $surfaceWater;
        $this->population = $population;
        $this->residents = $residents;
        $this->films = $films;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->url = $url;
    }

    /**
     * @param Object $planet
     * @return self
     */
    public static function fromEntity(
        Object $planet
    ): self {
        return new self(
            isset($planet->name) ? $planet->name : null,
            isset($planet->rotation_period) ? (int) $planet->rotation_period : null,
            isset($planet->orbital_period) ? (int) $planet->orbital_period : null,
            isset($planet->diameter) ? (int) $planet->diameter : null,
            isset($planet->climate) ? $planet->climate : null,
            isset($planet->gravity) ? $planet->gravity : null,
            isset($planet->terrain) ? $planet->terrain : null,
            isset($planet->surface_water) ? $planet->surface_water : null,
            isset($planet->population) ? (int) $planet->population : null,
            isset($planet->residents) ? $planet->residents : null,
            isset($planet->films) ? $planet->films : null,
            isset($planet->created) ? $planet->created : null,
            isset($planet->edited) ? $planet->edited : null,
            isset($planet->url) ? $planet->url : null,
        );
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getRotationPeriod(): ?int
    {
        return $this->rotationPeriod;
    }

    /**
     * @return int|null
     */
    public function getOrbitalPeriod(): ?int
    {
        return $this->orbitalPeriod;
    }

    /**
     * @return int|null
     */
    public function getDiameter(): ?int
    {
        return $this->diameter;
    }

    /**
     * @return string|null
     */
    public function getClimate(): ?string
    {
        return $this->climate;
    }

    /**
     * @return string|null
     */
    public function getGravity(): ?string
    {
        return $this->gravity;
    }

    /**
     * @return string|null
     */
    public function getTerrain(): ?string
    {
        return $this->terrain;
    }

    /**
     * @return string|null
     */
    public function getSurfaceWater(): ?string
    {
        return $this->surfaceWater;
    }

    /**
     * @return int|null
     */
    public function getPopulation(): ?int
    {
        return $this->population;
    }

    /**
     * @return array<int, string>|null
     */
    public function getResidents(): ?array
    {
        return $this->residents;
    }

    /**
     * @return array<int, string>|null
     */
    public function getFilms(): ?array
    {
        return $this->films;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
}