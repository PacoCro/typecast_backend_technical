<?php


namespace App\Representation;

/**
 * Class MovieRepresentation
 * @package App\Representation
 */
class MovieRepresentation
{

    /**
     * @var string|null
     */
    private ?string $title;

    /**
     * @var int|null
     */
    private ?int $episodesNumber;

    /**
     * @var string|null
     */
    private ?string $openingCrawl;

    /**
     * @var string|null
     */
    private ?string $director;

    /**
     * @var string|null
     */
    private ?string $producer;

    /**
     * @var string|null
     */
    private ?string $releaseDate;

    /**
     * @var array<int, string>|null
     */
    private ?array $characters;

    /**
     * @var array<int, string>|null
     */
    private ?array $planets;

    /**
     * @var array<int, string>|null
     */
    private ?array $starships;

    /**
     * @var array<int, string>|null
     */
    private ?array $vehicles;

    /**
     * @var array<int, string>|null
     */
    private ?array $species;

    /**
     * @var string|null
     */
    private ?string $createdAt;

    /**
     * @var string|null
     */
    private ?string $editedAt;

    /**
     * @var string|null
     */
    private ?string $url;

    /**
     * MovieRepresentation constructor.
     * @param string|null $title
     * @param int|null $episodesNumber
     * @param string|null $openingCrawl
     * @param string|null $director
     * @param string|null $producer
     * @param string|null $releaseDate
     * @param array<int, string>|null $characters
     * @param array<int, string>|null $planets
     * @param array<int, string>|null $starships
     * @param array<int, string>|null $vehicles
     * @param array<int, string>|null $species
     * @param string|null $createdAt
     * @param string|null $editedAt
     * @param string|null $url
     */
    public function __construct(
        ?string $title,
        ?int $episodesNumber,
        ?string $openingCrawl,
        ?string $director,
        ?string $producer,
        ?string $releaseDate,
        ?array $characters,
        ?array $planets,
        ?array $starships,
        ?array $vehicles,
        ?array $species,
        ?string $createdAt,
        ?string $editedAt,
        ?string $url
    )
    {
        $this->title = $title;
        $this->episodesNumber = $episodesNumber;
        $this->openingCrawl = $openingCrawl;
        $this->director = $director;
        $this->producer = $producer;
        $this->releaseDate = $releaseDate;
        $this->characters = $characters;
        $this->planets = $planets;
        $this->starships = $starships;
        $this->species = $species;
        $this->vehicles = $vehicles;
        $this->createdAt = $createdAt;
        $this->editedAt = $editedAt;
        $this->url = $url;
    }

    /**
     * @param Object $film
     * @return self
     */
    public static function fromEntity(
        object $film
    ): self {
        return new self(
            isset($film->title) ? $film->title : null,
            isset($film->episode_id) ? $film->episode_id : null,
            isset($film->opening_crawl) ? $film->opening_crawl : null,
            isset($film->director) ? $film->director : null,
            isset($film->producer) ? $film->producer : null,
            isset($film->release_date) ? $film->release_date : null,
            isset($film->characters) ? $film->characters : null,
            isset($film->planets) ? $film->planets : null,
            isset($film->starships) ? $film->starships : null,
            isset($film->vehicles) ? $film->vehicles : null,
            isset($film->species) ? $film->species : null,
            isset($film->created) ? $film->created : null,
            isset($film->edited) ? $film->edited : null,
            isset($film->url) ? $film->url : null
        );
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return int|null
     */
    public function getEpisodesNumber(): ?int
    {
        return $this->episodesNumber;
    }

    /**
     * @return string|null
     */
    public function getOpeningCrawl(): ?string
    {
        return $this->openingCrawl;
    }

    /**
     * @return string|null
     */
    public function getDirector(): ?string
    {
        return $this->director;
    }

    /**
     * @return string|null
     */
    public function getProducer(): ?string
    {
        return $this->producer;
    }

    /**
     * @return string|null
     */
    public function getReleaseDate(): ?string
    {
        return $this->releaseDate;
    }

    /**
     * @return string[]|null
     */
    public function getCharacters(): ?array
    {
        return $this->characters;
    }

    /**
     * @return string[]|null
     */
    public function getPlanets(): ?array
    {
        return $this->planets;
    }

    /**
     * @return string[]|null
     */
    public function getStarships(): ?array
    {
        return $this->starships;
    }

    /**
     * @return string[]|null
     */
    public function getVehicles(): ?array
    {
        return $this->vehicles;
    }

    /**
     * @return string[]|null
     */
    public function getSpecies(): ?array
    {
        return $this->species;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getEditedAt(): ?string
    {
        return $this->editedAt;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
}