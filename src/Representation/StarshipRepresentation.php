<?php


namespace App\Representation;


class StarshipRepresentation
{
    /**
     * @var string|null
     */
    private ?string $mglt;

    /**
     * @var int
     */
    private ?int $cargoCapacity;

    /**
     *
     */
    private ?string $consumables;

    /**
     * @var int|null
     */
    private ?int $costInCredits;

    /**
     * @var int|null
     */
    private ?int $crew;


    private ?float $hyperdriveRating;

    /**
     * @var string|null
     */
    private ?string $maxAtmospheringSpeed;

    /**
     * @var string|null
     */
    private ?string $model;

    /**
     * @var string|null
     */
    private ?string $name;

    /**
     * @var int|null
     */
    private ?int $passengers;

    /**
     * @var string[]|null
     */
    private ?array $films;

    /**
     * @var string[]|null
     */
    private ?array $pilots;

    /**
     * @var string|null
     */
    private ?string $starshipClass;

    /**
     * @var string|null
     */
    private ?string $manufacturer;

    /**
     * @var int|null
     */
    private ?int $length;

    /**
     * @var string|null
     */
    private ?string $createdAt;

    /**
     * @var string|null
     */
    private ?string $updatedAt;

    /**
     * @var string|null
     */
    private ?string $url;


    /**
     * StarshipRepresentation constructor.
     * @param string|null $mglt
     * @param int|null $cargoCapacity
     * @param string|null $consumables
     * @param int|null $costInCredits
     * @param int|null $crew
     * @param float|null $hyperdriveRating
     * @param string|null $maxAtmospheringSpeed
     * @param string|null $model
     * @param string|null $name
     * @param int|null $passengers
     * @param array<int, string>|null $films
     * @param array<int, string>|null $pilots
     * @param string|null $starshipClass
     * @param string|null $manufacturer
     * @param int|null $length
     * @param string|null $createdAt
     * @param string|null $updatedAt
     * @param string|null $url
     */
    public function __construct(
        ?string $mglt,
        ?int $cargoCapacity,
        ?string $consumables,
        ?int $costInCredits,
        ?int $crew,
        ?float $hyperdriveRating,
        ?string $maxAtmospheringSpeed,
        ?string $model,
        ?string $name,
        ?int $passengers,
        ?array $films,
        ?array $pilots,
        ?string $starshipClass,
        ?string $manufacturer,
        ?int $length,
        ?string $createdAt,
        ?string $updatedAt,
        ?string $url
    ) {
        $this->mglt = $mglt;
        $this->cargoCapacity = $cargoCapacity;
        $this->consumables = $consumables;
        $this->costInCredits = $costInCredits;
        $this->crew = $crew;
        $this->hyperdriveRating = $hyperdriveRating;
        $this->maxAtmospheringSpeed = $maxAtmospheringSpeed;
        $this->model = $model;
        $this->name = $name;
        $this->passengers = $passengers;
        $this->films = $films;
        $this->pilots = $pilots;
        $this->starshipClass = $starshipClass;
        $this->manufacturer = $manufacturer;
        $this->length = $length;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->url = $url;
    }

    /**
     * @param object $starship
     * @return self
     */
    public static function fromEntity(
        object $starship
    ): self {
        return new self(
            isset($starship->MGLT) ? $starship->MGLT : null,
            isset($starship->cargo_capacity) ? (int) $starship->cargo_capacity : null,
            isset($starship->consumables) ? $starship->consumables : null,
            isset($starship->cost_in_credits) ? (int) $starship->cost_in_credits: null,
            isset($starship->crew) ? (int) $starship->crew : null,
            isset($starship->hyperdrive_rating) ? (float) $starship->hyperdrive_rating : null,
            isset($starship->max_atmosphering_speed) ? $starship->max_atmosphering_speed : null,
            isset($starship->model) ? $starship->model : null,
            isset($starship->name) ? $starship->name : null,
            isset($starship->passengers) ? (int) $starship->passengers : null,
            isset($starship->films) ? $starship->films : null,
            isset($starship->pilots) ? $starship->pilots : null,
            isset($starship->starship_class) ? $starship->starship_class : null,
            isset($starship->manufacturer) ? $starship->manufacturer : null,
            isset($starship->length) ? $starship->length : null,
            isset($starship->created) ? $starship->created : null,
            isset($starship->edited) ? $starship->edited : null,
            isset($starship->url) ? $starship->url : null
        );
    }

    /**
     * @return string|null
     */
    public function getMglt(): ?string
    {
        return $this->mglt;
    }

    /**
     * @return int
     */
    public function getCargoCapacity(): ?int
    {
        return $this->cargoCapacity;
    }

    /**
     * @return int|null
     */
    public function getCostInCredits(): ?int
    {
        return $this->costInCredits;
    }

    /**
     * @return float|null
     */
    public function getHyperdriveRating(): ?float
    {
        return $this->hyperdriveRating;
    }

    /**
     * @return string|null
     */
    public function getMaxAtmospheringSpeed(): ?string
    {
        return $this->maxAtmospheringSpeed;
    }

    /**
     * @return string|null
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getPassengers(): ?int
    {
        return $this->passengers;
    }

    /**
     * @return string[]|null
     */
    public function getFilms(): ?array
    {
        return $this->films;
    }

    /**
     * @return string[]|null
     */
    public function getPilots(): ?array
    {
        return $this->pilots;
    }

    /**
     * @return string|null
     */
    public function getStarshipClass(): ?string
    {
        return $this->starshipClass;
    }

    /**
     * @return string|null
     */
    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

    /**
     * @return int|null
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getConsumables(): ?string
    {
        return $this->consumables;
    }

    /**
     * @return int|null
     */
    public function getCrew(): ?int
    {
        return $this->crew;
    }
}