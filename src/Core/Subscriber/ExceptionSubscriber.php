<?php


namespace App\Core\Subscriber;


use App\Core\Exception\ApiException;
use App\Core\Exception\ResponseEncodingException;
use App\Core\Representation\AssertionErrorRepresentation;
use App\Core\Representation\ExceptionRepresentation;
use App\Core\Service\ApiSerializer;
use Assert\LazyAssertionException;
use LogicException;
use Assert\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var ApiSerializer
     */
    private ApiSerializer $serializer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * ExceptionSubscriber constructor.
     * @param LoggerInterface $logger
     * @param ApiSerializer $serializer
     */
    public function __construct(
        LoggerInterface $logger,
        ApiSerializer $serializer
    ) {
        $this->logger       = $logger;
        $this->serializer   = $serializer;
    }

    /**
     * The events to listen to
     * @return array<string, array<int, string>>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['serializeOutput']
        ];
    }

    /**
     * @param ExceptionEvent $event
     * @throws ResponseEncodingException
     */
    public function serializeOutput(ExceptionEvent $event): void
    {
        $attributes = $event->getRequest()->attributes;
        $exception = $event->getThrowable();

        $headers = [];

        /**
         * Put more specific exceptions towards the top, ambiguous more towards the bottom
         */
        switch ($exception) {
            case $exception instanceof LazyAssertionException:
                $representation = new ExceptionRepresentation(
                    'ERR_INPUT_ASSERTION_FAILED'
                );
                $code = Response::HTTP_UNPROCESSABLE_ENTITY;

                $errors = array_map(
                    function (InvalidArgumentException $exception) {
                        return AssertionErrorRepresentation::fromAssertException($exception);
                    },
                    $exception->getErrorExceptions()
                );

                $representation->setErrors($errors);
                break;
            case $exception instanceof NotFoundHttpException:
                $representation = new ExceptionRepresentation(
                    'ERR_ENDPOINT_NOT_FOUND'
                );
                $code = Response::HTTP_NOT_FOUND;
                break;
            case $exception instanceof MethodNotAllowedHttpException:
                $representation = new ExceptionRepresentation(
                    'ERR_METHOD_NOT_ALLOWED'
                );
                $code = Response::HTTP_METHOD_NOT_ALLOWED;
                break;
            case $exception instanceof LogicException:
            case $exception instanceof ResponseEncodingException:
                $representation = new ExceptionRepresentation(
                    'ERR_INTERNAL_SERVER_ERROR'
                );
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;
            case $exception instanceof ApiException:
                // ApiException should be completely API-friendly
                $representation = new ExceptionRepresentation(
                    $exception->getMessage()
                );
                $code = $exception->getCode();
                $headers += $exception->getHeaders();
                break;
            case $exception instanceof AccessDeniedHttpException:
                $representation = new ExceptionRepresentation(
                    'ERR_ACCESS_DENIED'
                );
                $code = Response::HTTP_FORBIDDEN;
                break;
            default:
                $representation = new ExceptionRepresentation(
                    'ERR_UNHANDLED_EXCEPTION'
                );
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        /*
         * Unhandled exceptions are added to the logger
         */
        if ($code === Response::HTTP_INTERNAL_SERVER_ERROR) {
            $this->logger->error(
                sprintf(
                    "Code: %s; Message: %s;",
                    $exception->getCode(),
                    $exception->getMessage()
                )
            );
        }

        $contentType        = $attributes->get('ContentType', 'application/json');
        $contentTypeFormat  = $attributes->get('ContentTypeFormat', 'json');
        $contentEncoding    = $attributes->get('ContentEncoding', 'identity');

        $body = $this->serializer->serialize(
            $representation,
            $contentTypeFormat,
            $contentEncoding
        );

        $response = new Response(
            $body,
            $code,
            [
                'Content-Type'      => $contentType,
                'Content-Encoding'  => $contentEncoding
            ] + $headers
        );

        $event->setResponse($response);
        $event->stopPropagation();
    }
}