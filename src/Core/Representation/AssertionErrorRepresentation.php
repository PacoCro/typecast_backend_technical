<?php


namespace App\Core\Representation;


use Assert\InvalidArgumentException;

/**
 * Class AssertionErrorRepresentation
 * @package App\Core\Representation
 */
class AssertionErrorRepresentation
{
    /**
     * @var string
     */
    private string $message;

    /**
     * @var array<mixed>
     */
    private array $constraints;

    /**
     * AssertErrorRepresentation constructor.
     * @param string $message
     * @param array<mixed> $constraints
     */
    public function __construct(
        string $message,
        array $constraints
    ) {
        $this->message = $message;
        $this->constraints = $constraints;
    }

    /**
     * @param InvalidArgumentException $exception
     * @return AssertionErrorRepresentation
     */
    public static function fromAssertException(InvalidArgumentException $exception): self
    {
        return new self(
            $exception->getMessage(),
            $exception->getConstraints()
        );
    }
}