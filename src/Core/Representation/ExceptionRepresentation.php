<?php


namespace App\Core\Representation;

/**
 * Class ExceptionRepresentation
 * @package App\Core\Representation
 */
class ExceptionRepresentation
{
    /**
     * @var string
     */
    private string $message;

    /**
     * Array of error representations for multi-error response
     * @var object[]|null
     */
    private ?array $errors = null;

    /**
     * ExceptionRepresentation constructor.
     * @param string $message
     */
    public function __construct(
        string $message
    ) {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }


    /**
     * @param array<mixed> $errors
     * @return ExceptionRepresentation
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return object[]|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }
}