<?php


namespace App\Core\Service;

use JMS\Serializer\SerializerInterface;
use App\Core\Exception\ResponseEncodingException;

/**
 * Class ApiSerializer
 * @package App\Core\Service
 */
class ApiSerializer
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * ApiSerializer constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    /**
     * @param object $representation
     * @param string $format
     * @param string $encoding
     * @return string
     * @throws ResponseEncodingException
     */
    public function serialize(
        object $representation,
        string $format,
        string $encoding
    ): string {
        $body = $this->serializer->serialize(
            $representation,
            $format
        );

        // Add encoding support here
        switch ($encoding) {
            case 'x-gzip':
            case 'gzip':
                $body = gzencode($body, 9, FORCE_GZIP);
                break;
            case 'deflate':
                $body = gzencode($body, 9, FORCE_DEFLATE);
                break;
        }

        if ($body === false) {
            throw new ResponseEncodingException();
        }

        return $body;
    }
}