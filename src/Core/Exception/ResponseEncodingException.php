<?php


namespace App\Core\Exception;


use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseEncodingException
 * @package App\Core\Exception
 */
class ResponseEncodingException extends ApiException
{
    /**
     * ResponseEncodingException constructor.
     */
    public function __construct()
    {
        parent::__construct(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            'ERR_RESPONSE_ENCODING_FAILED'
        );
    }
}