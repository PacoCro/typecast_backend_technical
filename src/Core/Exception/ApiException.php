<?php


namespace App\Core\Exception;


use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class ApiException
 * @package App\Core\Exception
 */
class ApiException extends Exception
{
    /**
     * @var array<string, string>
     */
    protected array $headers = [];

    /**
     * ApiException constructor.
     * @param int $httpCode
     * @param string $message
     * @param array<string, string> $headers
     * @param Throwable|null $previous
     */
    public function __construct(
        int $httpCode = Response::HTTP_SERVICE_UNAVAILABLE,
        string $message = '',
        array $headers = [],
        Throwable $previous = null
    ) {
        parent::__construct($message, $httpCode, $previous);
        $this->headers = $headers;
    }

    /**
     * @return array<string, string>
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}