<?php

namespace App\Controller;

use App\Representation\MovieRepresentation;
use App\Service\Swapi\SwapiClient;
use Assert\Assert;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MoviesController
 * @package App\Controller
 */
class MoviesController extends AbstractController
{
    /**
     * @Route("/movie", name="get_movies", methods={"GET"})
     * @param Request $request
     * @param SwapiClient $swapiClient
     * @return Response
     * @throws GuzzleException
     */
    public function index(
        Request $request,
        SwapiClient $swapiClient
    ): Response {
        $filter = $request->get('filter');
        $characterId = isset($filter['characterId']) ? $filter['characterId'] : null;
        $searchTitle = isset($filter['title']) ? $filter['title'] : null;

        $this->assertGetMoviesInput(
            $characterId,
            $searchTitle
        );

        $filter = [
          'characterId' => $characterId !== null ? (int) $characterId : null
        ];

        $response = $swapiClient->getFilms($searchTitle);
        $films = $response->results;

        $movieRepresentations = [];
        foreach ($films as $movie) {
            if ($filter['characterId'] !== null) {
                foreach($movie->characters as $character) {
                    if (strpos($character, 'people/' . $filter['characterId'] . '/')) {
                        $movieRepresentations[] = MovieRepresentation::fromEntity($movie);
                        continue;
                    }
                }
            } else {
                $movieRepresentations[] =  MovieRepresentation::fromEntity($movie);
            }
        }

        $count = count($movieRepresentations);
        return $this->json([
            'page' => 1,
            'limit' => $count,
            'pages' => 1,
            'total' => $count,
            'items' => $movieRepresentations
        ]);
    }

    /**
     * @param string|null $characterId
     */
    private function assertGetMoviesInput(
        ?string $characterId,
        ?string $searchTitle
    ): void {
        Assert::lazy()
            ->that($characterId)
            ->nullOr()
            ->numeric('CharacterId must be integer.')
            ->greaterThan(0, 'CharacterId must be positive number.')
            ->that($searchTitle)
            ->nullOr()
            ->string('Title must be string.')
            ->verifyNow();
    }
}
