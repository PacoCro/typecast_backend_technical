<?php

namespace App\Controller;

use App\Representation\StarshipRepresentation;
use App\Service\Swapi\SwapiClient;
use Assert\Assert;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StarshipsController
 * @package App\Controller
 */
class StarshipsController extends AbstractController
{
    /**
     * @Route("/starship", name="get_starships", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(
        Request $request
    ): Response {
        $filter = $request->get('filter');
        $model = isset($filter['model']) ? $filter['model'] : null;
        $name = isset($filter['name']) ? $filter['name'] : null;
        $passengerNumberFrom = isset($filter['passengerNumberFrom']) ? $filter['passengerNumberFrom'] : null;

        $this->assertGetStarshipsInput(
            $model,
            $name,
            $passengerNumberFrom
        );

        $filter = [
            'model' => $model,
            'name' => $name,
            'passengerNumberFrom' => (int) $passengerNumberFrom
        ];

        $starshipRepresentations = $this->getFilteredStarships($filter);

        $count = count($starshipRepresentations);
        return $this->json([
            'page' => 1,
            'limit' => $count,
            'pages' => 1,
            'total' => $count,
            'items' => $starshipRepresentations
        ]);
    }

    /**
     * @param string|null $model
     * @param string|null $name
     * @param string|null $passengerNumberFrom
     */
    private function assertGetStarshipsInput(
        ?string $model,
        ?string $name,
        ?string $passengerNumberFrom
    ): void {
        Assert::lazy()
            ->that($model)
            ->nullOr()
            ->string('Model must be string.')
            ->that($name)
            ->nullOr()
            ->string('Name must be string.')
            ->that($passengerNumberFrom)
            ->nullOr()
            ->numeric('PassengerNumberFrom must be integer.')
            ->greaterThan(0, 'PassengerNumberFrom must be positive number.')
            ->verifyNow();
    }

    /**
     * @param array<string, mixed> $filter
     * @return StarshipRepresentation[]
     * @throws GuzzleException
     */
    private function getFilteredStarships(
        array $filter
    ): array {
        $swapiClient = new SwapiClient();

        $starshipRepresentations = [];
        $counter = 1;
        do {
            $response = $swapiClient->getStarships(
                $counter,
                $filter['name'],
                $filter['model']
            );
            $starships = $response->results;

            foreach ($starships as $starship) {
                if ($filter['passengerNumberFrom'] !== null) {
                    if ($starship->passengers > $filter['passengerNumberFrom']) {
                        $starshipRepresentations[] =  StarshipRepresentation::fromEntity($starship);
                    }
                } else {
                    $starshipRepresentations[] =  StarshipRepresentation::fromEntity($starship);
                }
            }
            $counter++;
        } while ($response->next);

        return $starshipRepresentations;
    }
}
