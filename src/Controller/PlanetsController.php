<?php

namespace App\Controller;

use App\Representation\PlanetRepresentation;
use App\Service\Swapi\SwapiClient;
use Assert\Assert;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlanetsController
 * @package App\Controller
 */
class PlanetsController extends AbstractController
{
    /**
     * @Route("/planet", name="get_planets", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(
        Request $request
    ): Response {
        $filter = $request->get('filter');
        $dateFrom = isset($filter['dateFrom']) ? $filter['dateFrom'] : null;
        $name = isset($filter['name']) ? $filter['name'] : null;

        $this->assertGetPlanetsInput(
            $dateFrom,
            $name
        );

        $filter = [
            'dateFrom' => $dateFrom !== null ? new DateTime($dateFrom) : null,
            'name' => $name
        ];

        $planetRepresentations = $this->getFilteredPlanets($filter);

        $count = count($planetRepresentations);
        return $this->json([
            'page' => 1,
            'limit' => $count,
            'pages' => 1,
            'total' => $count,
            'items' => $planetRepresentations
        ]);
    }

    /**
     * @param string|null $dateFrom
     * @param string|null $name
     */
    private function assertGetPlanetsInput(
        ?string $dateFrom,
        ?string $name
    ): void {
        Assert::lazy()
            ->that($dateFrom)
            ->nullOr()
            ->string('DateFrom must be string.')
            ->regex("/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/")
            ->that($name)
            ->nullOr()
            ->string('Name must be string.')
            ->verifyNow();
    }

    /**
     * @param array<string, mixed> $filter
     * @return PlanetRepresentation[]
     * @throws GuzzleException
     */
    private function getFilteredPlanets(array $filter): array {
        $swapiClient = new SwapiClient();

        $planetRepresentations = [];

        $counter = 1;
        do {
            $response = $swapiClient->getPlanets($counter, $filter['name']);
            $planets = $response->results;

            foreach ($planets as $planet) {
                if ($filter['dateFrom'] !== null) {
                    $planetCreatedAt = new DateTime($planet->created);
                    if ($planetCreatedAt > $filter['dateFrom']) {
                        $planetRepresentations[] =  PlanetRepresentation::fromEntity($planet);
                    }
                } else {
                    $planetRepresentations[] =  PlanetRepresentation::fromEntity($planet);
                }
            }
            $counter++;
        } while ($response->next);

        return $planetRepresentations;
    }
}
