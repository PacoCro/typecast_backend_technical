<?php


namespace App\Service\Swapi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class SwapiClient
 * @package App\Service\Swapi
 */
class SwapiClient
{
    public const SWAPI_BASE_URL = 'https://swapi.dev/api/';

    /**
     * @var Client
     */
    private Client $client;

    /**
     * SwapiClient constructor.
     */
    public function __construct() {
        $this->client = new Client([
            'base_uri' => self::SWAPI_BASE_URL
        ]);
    }

    /**
     * @param string|null $searchTitle
     * @return mixed
     * @throws GuzzleException
     */
    public function getFilms(
        ?string $searchTitle = null
    ) {
        $url = 'films';
        $query = [];

        if ($searchTitle) {
            $query['search'] = $searchTitle;
        }

        return json_decode($this->client->request('GET', $url, [
            'query' => $query
        ])->getBody()->getContents());
    }

    /**
     * @param int $page
     * @param string|null $searchName
     * @return mixed
     * @throws GuzzleException
     */
    public function getPlanets(
        int $page = 1,
        ?string $searchName = null
    ) {
        $url = 'planets';
        $query['page'] = $page;

        if ($searchName) {
            $query['search'] = $searchName;
        }

        return json_decode($this->client->request('GET', $url, [
            'query' => $query
        ])->getBody()->getContents());
    }

    /**
     * @param int $page
     * @param string|null $searchName
     * @param string|null $model
     * @return mixed
     * @throws GuzzleException
     */
    public function getStarships(
        int $page = 1,
        ?string $searchName = null,
        ?string $model = null
    ) {
        $url = 'starships';
        $query['page'] = $page;

        if ($searchName) {
            $query['name'] = $searchName;
        } elseif ($model) {
            $query['model'] = $model;
        }

        return json_decode($this->client->request('GET', $url, [
            'query' => $query
        ])->getBody()->getContents());
    }
}
