# Installation

- Prepare your environment to run php 7.4 with Symfony (https://symfony.com/doc/current/setup.html)
- Pull project from Gitlab (https://gitlab.com/PacoCro/typecast_backend_technical.git) - I can send zip with files or give you permissions to repo.
- Navigate to project root folder and execute "composer install"
- If there are errors, fix errors by installing arr requirements from composer.json file.
- Import endpoints to Postman (from Postman folder)
- Suggestion: Setup up your project to run on extendedswapi.test local domain name.
- If you run project from some other domain name or localhost, make sure to replace hostname variable in Postman to your own domain name.


# Documentation

You can access project api documentation if project is run in dev environment on http://{your-hostname}/docs/http://extendedswapi.test/docs/api.yaml

# Task details

### API TEST CASE:
Using the Star Wars API: https://swapi.dev extend it with new endpoints and add in a form of some API client for below:

- Show all films where involved a given character acts **DONE**
- Show all planets created after 12/04/2014 **DONE**
- Show people starships which have above 84000 passengers in total **DONE**
- Showing all information of a Films model in as endpoint and then on FRONTEND display it in table using HTML/CSS and some JS component (jQuery, VueJS, React or Angular). Be free to use some cool 3rd party library for Table component. **MISSING - OUT OF THE SCOPE**

### EXECUTION:
Please make it in some of the modern PHP Frameworks: preferably in Laravel or **Symfony**. Make sure it is something you're proud of and shows us your knowledge and abilities. You are free to use some 3rd party packages and be creative on your own.

Also, keep in mind these points your application to have:

- Use awesome techniques
- Do some API docs or POSTMAN JSON collection share
- Make it (the code) look pretty: extensible, readable …
- Make sure your API Client has filtering and search options included as in API Docs stated

### Bonus points:

Want to impress us a bit more? Here are some tips to do so:

- Dockerize it **I know how to work with docker but I was out of time**
- Make some unit tests by TDD Framework of your choice **I worked in TDD environemnt but I was out of time**
- Extend REST API for above endpoints to support Graph QL as well **I never worked with Graph QL**
